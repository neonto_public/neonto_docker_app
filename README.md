# Neonto Services

This repository contains a docker setup to run the Neonto application. It provides a user interface that enables you to
measure your ontologies and knowledge graphs. So far, we support public Git-repositories and SPARQL-Endpoints.

## Prerequisites

- Docker
- 16GB of RAM (8GB might work, but can be extremely slow)
- Your individual access token for Docker

## Installation and Setup

1. Configure the .env file. 
   - APP_EMAIL: The email and username for the application that you will use to log in.
   - APP_PASSWORD: The password for the application that you will use to log in.
   - DB_USERNAME: The username for the database.
   - DB_PASSWORD: The password for the database.
   - (Optional) SERVER_HOST: The host on which the server will run. Only change if you are not running the server on localhost.
   - (Optional) SERVER_PORT: The port on which the server will run. You don't need to change this.
2. Login to docker: `docker login -u neonto`
3. When you are asked for a password, use the access token we provided to you. 
4. Start the docker services: `docker compose up -d`

## Usage

Wait roundabout 15 minutes until all services were started successfully. Afterward visit [http://localhost:8888](http://localost:8888). Don't forget to log in first. The username and password are
as provided in the .env file. The user guidance is rudimentary so far.

## License

This software is licensed under a custom license granted by Neonto GbR. By using this software, you agree to the terms and conditions of this license.

Neonto GbR reserves the right to revoke this license at any time and prohibit the user from further using the software. For more information, please contact Neonto GbR.

This license does not permit any form of redistribution or commercial use without explicit permission from Neonto GbR.

## Contact 

If you have any questions or need help, please contact us at [achim.reiz@neonto.de](mailto:achim.reiz@neonto.de) or [pascal.sommer@neonto.de](mailto:pascal.sommer@neonto.de)